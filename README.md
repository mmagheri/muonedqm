# muonedqm

muone tb dqm with plotly

## Servering

server with just 

```
python3 app.py 
```

## Starting the container

``````
docker pull gitlab-registry.cern.ch/mmagheri/muonedqm
docker run -d -p 8050:8050 --name dqmtest gitlab-registry.cern.ch/mmagheri/muonedqm
docker exec -it dqmtest /bin/bash
```

## Starting the container

In order to run the job manager, you just need to run:

```
python3 workManager.py -f EXPRESS_INPUT_FILE_DIRECTORY -o OUTPUT_FILE_DIRECTORY -x muetrackreco/Structure/MUonEStructure_TB2022_reissued.xml 
```

## Starting the server


```
python3 app.py 
```

you'll need to match the OUTPUT_FILE_DIRECTORY from the workmanager and the input folder in line 14 of pages/dqm.py
``````