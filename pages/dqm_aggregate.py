import dash
from dash import html, dcc, callback, Input, Output
import sys
sys.path.insert(1, '../')
sys.path.insert(1, '/muonedqm/exporter')
from container import RootFileHistograms
from exporter.alignmentContainer import AlignParamContainer
from exporter.efficiencyContainer import EfficiencyContainer
from pythonUtils.fileFunctions import find_latest_file_in_folder, get_latest_folder
import os


dash.register_page(__name__)

input_folder = '/muonedqm/newOutputTest'
list_run = os.listdir(input_folder)

# Load the histogram
root_container = RootFileHistograms()
align_param = AlignParamContainer()
eff_container = EfficiencyContainer()
beam_profiles = ['beamProfile_1', 'beamProfile_2', 'beamProfile_3', 'beamProfile_4', 'beamProfile_5', 'beamProfile_6', 'beamProfile_7', 'beamProfile_8', 'beamProfile_9', 'beamProfile_10', 'beamProfile_11', 'beamProfile_12'] 
bend = ['bend_1', 'bend_2', 'bend_3', 'bend_4', 'bend_5', 'bend_6', 'bend_7', 'bend_8', 'bend_9', 'bend_10', 'bend_11', 'bend_12']
beam_spot = ['2DProfile_0_1', '2DProfile_2_3', '2DProfile_4_5', '2DProfile_6_7', '2DProfile_8_9', '2DProfile_10_11']
residuals = ['hDistanceFromInterceptedPointX_0', 
             'hDistanceFromInterceptedPointY_1', 
             'hDistanceFromInterceptedPointX_2', 
             'hDistanceFromInterceptedPointY_2', 
             'hDistanceFromInterceptedPointX_3', 
             'hDistanceFromInterceptedPointY_3', 
             'hDistanceFromInterceptedPointX_4', 
             'hDistanceFromInterceptedPointY_5',
             #'hDistanceFromInterceptedPointX_6', 
             #'hDistanceFromInterceptedPointY_7', 
             #'hDistanceFromInterceptedPointX_8', 
             #'hDistanceFromInterceptedPointY_8', 
             #'hDistanceFromInterceptedPointX_9', 
             #'hDistanceFromInterceptedPointY_9', 
             #'hDistanceFromInterceptedPointX_10', 
             #'hDistanceFromInterceptedPointY_11'
             ]

root_container.empty_histogram_1d(beam_profiles + bend + beam_spot + residuals)
eff_container.empty_efficiency()
align_param.empty_alignment()

eff_div = eff_container.efficiency_to_dash()
align_param_div = align_param.positions_to_dash_div()
beam_graph = root_container.to_dash_graph(beam_profiles)
bend_graph = root_container.to_dash_graph(bend)
beam_spot_graph = root_container.to_dash_graph(beam_spot)
residuals_graph = root_container.to_dash_graph(residuals)

layout = html.Div([
    html.H1('The dqm page'),
    html.Div([
        dcc.Dropdown(list_run, list_run[0], id='run-dropdown'),
        html.Div(id='chosen-run')
    ]),
    dcc.Tabs([
        dcc.Tab(label='Beam Profiles', children=[
            html.Div(id='beam-profile-container-aggregate', children=beam_graph)
        ]),
        dcc.Tab(label='Bend', children=[
            html.Div(id='bend-container-aggregate', children=bend_graph)
        ]),
        dcc.Tab(label='Beam spot', children=[
            html.Div(id='beam-spot-container-aggregate', children=beam_spot_graph)
        ]),
        dcc.Tab(label='Residuals', children=[
            html.Div(id='residuals-container-aggregate', children=residuals_graph)
        ]),
        dcc.Tab(label='AlignParam', children=[
            html.Div(id='align-param-aggregate', children=align_param_div)
        ]),
        dcc.Tab(label='Efficiency', children=[
            html.Div(id='efficiency-aggregate', children=eff_div)
        ]),

    ]),
])

@callback(
    [
        Output('beam-profile-container-aggregate', 'children'),
        Output('bend-container-aggregate', 'children'),
        Output('beam-spot-container-aggregate', 'children'),
        Output('residuals-container-aggregate', 'children'),
        Output('align-param-aggregate', 'children'),
        Output('efficiency-aggregate', 'children'),
    ],
    Input('run-dropdown', 'value')
)

def update_aggregate_plot(value):
    run_path = input_folder + '/' + value
    root_container.clear()

    # Update the container with the latest file
    root_container.aggregate_histograms(run_path + "/SimpleHisto", beam_profiles + bend + beam_spot)
    root_container.aggregate_histograms(run_path + "/singleTrackPlots", residuals)

    for run_file in os.listdir(run_path+"/alignment"):
        align_param.load_from_file(run_path+"/alignment" + '/' + run_file)

    for run_file in os.listdir(run_path+"/efficiency"):
        eff_container.load_file(run_path+"/efficiency" + '/' + run_file)
    # ... Load other histograms
    
    # Generate updated graphs
    beam_graph_updated = root_container.to_dash_graph(beam_profiles)
    bend_graph_updated = root_container.to_dash_graph(bend)
    beam_spot_graph_updated = root_container.to_dash_graph(beam_spot)
    residuals_graph_updated = root_container.to_dash_graph(residuals)
    align_param_div_updated = align_param.positions_to_dash_div()
    eff_div_updated = eff_container.efficiency_to_dash()


    return beam_graph_updated, bend_graph_updated, beam_spot_graph_updated, residuals_graph_updated, align_param_div_updated, eff_div_updated  # adjust these to match your graph updates