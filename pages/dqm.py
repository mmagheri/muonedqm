import dash
from dash import html, dcc, callback, Input, Output
import sys
sys.path.insert(1, '../')
sys.path.insert(1, '/muonedqm/exporter')
from container import RootFileHistograms
from exporter.alignmentContainer import AlignParamContainer
from exporter.efficiencyContainer import EfficiencyContainer
from pythonUtils.fileFunctions import find_latest_file_in_folder, get_latest_folder


dash.register_page(__name__)

input_folder = '/muonedqm/newOutputTest'
latest_folder = get_latest_folder(input_folder)

# Load the histogram
root_container = RootFileHistograms()
align_param = AlignParamContainer()
eff_container = EfficiencyContainer()
beam_profiles = ['beamProfile_1', 'beamProfile_2', 'beamProfile_3', 'beamProfile_4', 'beamProfile_5', 'beamProfile_6', 'beamProfile_7', 'beamProfile_8', 'beamProfile_9', 'beamProfile_10', 'beamProfile_11', 'beamProfile_12'] 
bend = ['bend_1', 'bend_2', 'bend_3', 'bend_4', 'bend_5', 'bend_6', 'bend_7', 'bend_8', 'bend_9', 'bend_10', 'bend_11', 'bend_12']
beam_spot = ['2DProfile_0_1', '2DProfile_2_3', '2DProfile_4_5', '2DProfile_6_7', '2DProfile_8_9', '2DProfile_10_11']
residuals = ['hDistanceFromInterceptedPointX_0', 
             'hDistanceFromInterceptedPointY_1', 
             'hDistanceFromInterceptedPointX_2', 
             'hDistanceFromInterceptedPointY_2', 
             'hDistanceFromInterceptedPointX_3', 
             'hDistanceFromInterceptedPointY_3', 
             'hDistanceFromInterceptedPointX_4', 
             'hDistanceFromInterceptedPointY_5',
             #'hDistanceFromInterceptedPointX_6', 
             #'hDistanceFromInterceptedPointY_7', 
             #'hDistanceFromInterceptedPointX_8', 
             #'hDistanceFromInterceptedPointY_8', 
             #'hDistanceFromInterceptedPointX_9', 
             #'hDistanceFromInterceptedPointY_9', 
             #'hDistanceFromInterceptedPointX_10', 
             #'hDistanceFromInterceptedPointY_11'
             ]

root_container.load_histograms(find_latest_file_in_folder(latest_folder + "/SimpleHisto"), beam_profiles + bend + beam_spot)
root_container.load_histograms(find_latest_file_in_folder(latest_folder + "/singleTrackPlots"), residuals)
eff_container.load_from_directory(latest_folder + "/efficiency")
align_param.load_from_directory(latest_folder + "/alignment")

eff_div = eff_container.efficiency_to_dash()
align_param_div = align_param.positions_to_dash_div()
beam_graph = root_container.to_dash_graph(beam_profiles)
bend_graph = root_container.to_dash_graph(bend)
beam_spot_graph = root_container.to_dash_graph(beam_spot)
residuals_graph = root_container.to_dash_graph(residuals)

layout = html.Div([
    html.H1('The dqm page'),
    dcc.Interval(
        id='interval-component',
        interval=5*1000,  # in milliseconds (5 seconds)
        n_intervals=0
    ),
    dcc.Tabs([
        dcc.Tab(label='Beam Profiles', children=[
            html.Div(id='beam-profile-container', children=beam_graph)
        ]),
        dcc.Tab(label='Bend', children=[
            html.Div(id='bend-container', children=bend_graph)
        ]),
        dcc.Tab(label='Beam spot', children=[
            html.Div(id='beam-spot-container', children=beam_spot_graph)
        ]),
        dcc.Tab(label='Residuals', children=[
            html.Div(id='residuals-container', children=residuals_graph)
        ]),
        dcc.Tab(label='AlignParam', children=[
            html.Div(id='align-param', children=align_param_div)
        ]),
        dcc.Tab(label='Efficiency', children=[
            html.Div(id='efficiency', children=eff_div)
        ]),

    ]),
])

@callback(
    [
        Output('beam-profile-container', 'children'),
        Output('bend-container', 'children'),
        Output('beam-spot-container', 'children'),
        Output('residuals-container', 'children'),
        Output('align-param', 'children'),
        Output('efficiency', 'children'),
    ],
    Input('interval-component', 'n_intervals')
)
def update_plot(n_intervals):
    # Check the signal (e.g., reading a file)
    with open('signal.txt', 'r') as file:
        signal = file.read()
    
    if signal != 'refresh':
        # If no refresh signal, return dash.no_update to prevent unnecessary re-rendering
        return [dash.no_update] * 6  # or however many graphs you have

    # Reset the signal
    with open('signal.txt', 'w') as file:
        file.write('')  # reset the signal

    print("Refreshing graphs...")
    latest_folder = get_latest_folder(input_folder)  # make sure you define this function
    root_container.clear()
    
    # Update the container with the latest file
    root_container.load_histograms(latest_folder, beam_profiles + bend)
    root_container.load_histograms(find_latest_file_in_folder(latest_folder + "/SimpleHisto"), beam_profiles + bend + beam_spot)
    root_container.load_histograms(find_latest_file_in_folder(latest_folder + "/singleTrackPlots"), residuals)
    align_param.load_from_file(find_latest_file_in_folder(latest_folder + "/alignment"))
    eff_container.load_file(find_latest_file_in_folder(latest_folder + "/efficiency"))
    # ... Load other histograms
    
    # Generate updated graphs
    beam_graph_updated = root_container.to_dash_graph(beam_profiles)
    bend_graph_updated = root_container.to_dash_graph(bend)
    beam_spot_graph_updated = root_container.to_dash_graph(beam_spot)
    residuals_graph_updated = root_container.to_dash_graph(residuals)
    align_param_div_updated = align_param.positions_to_dash_div()
    eff_div_updated = eff_container.efficiency_to_dash()

    return beam_graph_updated, bend_graph_updated, beam_spot_graph_updated, residuals_graph_updated, align_param_div_updated, eff_div_updated  # adjust these to match your graph updates
