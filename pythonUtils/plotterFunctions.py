import os
import threading
import time
import subprocess
    
def makeSimpleHisto(file_path, output_file_path):
    # Path to the precompiled C++ program
    cpp_program_path = "/muonedqm/muonedqmcplotter/build/apps/FastPlotter"
    
    # Prepare the command
    cmd_list = [cpp_program_path, '-i', file_path, '-o', output_file_path]
    cmd_string = ' '.join(cmd_list)

    # Print the command
    print(cmd_string)
    
    # Run the C++ program
    result = subprocess.run(cmd_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    # Check for errors
    if result.returncode != 0:
        print(f"Error running C++ makeSimpleHisto program on file {file_path}: {result.stderr.decode('utf-8')}")
        
    else:
        print(f"Successfully processed file {file_path} with C++ program: {result.stdout.decode('utf-8')}")
    return output_file_path

def makeSingleTrackPlots(file_path, output_file_path, xmlPath):
    # Path to the precompiled C++ program
    cpp_program_path = "/muonedqm/muetrackreco/build/apps/singleTracker"

    # Prepare the command
    cmd_list = [cpp_program_path, '-i', file_path, '-a', '-o', output_file_path, '-x', xmlPath]
    cmd_string = ' '.join(cmd_list)

    # Print the command
    print(cmd_string)

    # Run the C++ program
    result = subprocess.run(cmd_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    # Check for errors
    if result.returncode != 0:
        print(f"Error running C++ program makeSingleTrackPlots on file {file_path}: {result.stderr.decode('utf-8')}")
        
    else:
        print(f"Successfully processed file {file_path} with C++ program: {result.stdout.decode('utf-8')}")
    return output_file_path

def makeMultiTrackPlots(file_path, output_file_path, xmlPath):
    # Path to the precompiled C++ program
    cpp_program_path = "/muonedqm/muetrackreco/build/apps/multiTracker"

    # Prepare the command
    cmd_list = [cpp_program_path, '-i', file_path, '-o', output_file_path, '-x', xmlPath, '-j']
    cmd_string = ' '.join(cmd_list)

    # Print the command
    print(cmd_string)

    # Run the C++ program
    result = subprocess.run(cmd_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    # Check for errors
    if result.returncode != 0:
        print(f"Error running C++ program makeMultiTrackPlots on file {file_path}: {result.stderr.decode('utf-8')}")
    else:
        print(f"Successfully processed file {file_path} with C++ program: {result.stdout.decode('utf-8')}")
    return output_file_path


def align_function(file_path, output_file_path, xmlPath):
    # Path to the precompiled C++ program
    cpp_program_path = "/muonedqm/muetrackreco/build/apps/aligner"

    # Prepare the command
    cmd_list = [cpp_program_path, '-i', file_path, '-a', '-o', output_file_path, '-x', xmlPath, '-a']
    cmd_string = ' '.join(cmd_list)

    # Print the command
    print(cmd_string)

    # Run the C++ program
    result = subprocess.run(cmd_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    # Check for errors
    if result.returncode != 0:
        print(f"Error running alignFunction C++ program on file {file_path}: {result.stderr.decode('utf-8')}")
        
    else:
        print(f"Successfully processed file {file_path} with C++ program: {result.stdout.decode('utf-8')}")
    return output_file_path

def calculateEfficiency(file_path, output_file_path, xmlPath):
    # Path to the precompiled C++ program
    cpp_program_path = "/muonedqm/muetrackreco/build/apps/efficiency"

    # Prepare the command
    cmd_list = [cpp_program_path, '-i', file_path, '-o', output_file_path, '-x', xmlPath]
    cmd_string = ' '.join(cmd_list)

    # Print the command
    print(cmd_string)

    # Run the C++ program
    result = subprocess.run(cmd_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    # Check for errors
    if result.returncode != 0:
        print(f"Error running C++ program on file {file_path}: {result.stderr.decode('utf-8')}")
        
    else:
        print(f"Successfully processed file {file_path} with C++ program: {result.stdout.decode('utf-8')}")
    return output_file_path