import os

def get_latest_folder(path="."):
    """
    Get the latest created folder in the given path.
    """
    # List all entries in the path
    all_entries = os.listdir(path)

    # Filter for only directories
    all_folders = [d for d in all_entries if os.path.isdir(os.path.join(path, d))]

    # Find the latest folder based on creation time
    latest_folder = max(all_folders, key=lambda d: os.path.getctime(os.path.join(path, d)))
    
    return os.path.join(path, latest_folder)

def find_latest_file_in_folder(directory):
    files = [os.path.join(directory, f) for f in os.listdir(directory)]
    files = [f for f in files if os.path.isfile(f)]
    if not files:
        return None
    return max(files, key=os.path.getctime)

def setupFolder(base_path):
    """
    Create specific directories inside the base_path if they don't already exist.
    """
    directories = ['SimpleHisto', 'EventDisplay', 'SimpleHisto', 'singleTrackPlots', 'alignment', 'efficiency']
    
    for directory in directories:
        dir_path = os.path.join(base_path, directory)
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)

def create_corresponding_folder(input_path, output_path):
    """
    Create a folder in the output path that corresponds to the folder name in the input path.
    If the folder name from the input path already exists in the output path, the directory will not be created.
    """
    # Extract the folder name from the input path
    folder_name = os.path.basename(input_path)
    
    # Create the full path for the new directory
    new_dir_path = os.path.join(output_path, folder_name)

    # Check if the directory doesn't exist in the output path, then create it
    if not os.path.exists(new_dir_path) and folder_name not in os.listdir(output_path):
        os.makedirs(new_dir_path)
        setupFolder(new_dir_path)
        
    return new_dir_path
