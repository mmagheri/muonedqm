import threading
import time
import argparse
import os
from pythonUtils.plotterFunctions import makeSimpleHisto, makeSingleTrackPlots, makeMultiTrackPlots, calculateEfficiency, align_function
from pythonUtils.fileFunctions import find_latest_file_in_folder, get_latest_folder, create_corresponding_folder


def main():
    parser = argparse.ArgumentParser(description="Process the latest root file in the specified folder.")
    parser.add_argument('-f', '--folder', type=str, required=True, help='Path to the folder to monitor')
    parser.add_argument('-o', '--output', type=str, required=True, help='Path to the output folder to monitor')
    parser.add_argument('-x', '--xml', type=str, required=True, help='Path to the xml settings file for exp setup')

    args = parser.parse_args()
    folder = args.folder
    base_output_folder = args.output
    xml_file = args.xml

    latest_processed_file = None

    start_time = time.time()

    firstTime = True
    while True:
        print("Latest folder: ", get_latest_folder(folder))
        latest_file = find_latest_file_in_folder(get_latest_folder(folder))
        file_name_without_extension = os.path.splitext(os.path.basename(latest_file))[0]
        print("Latest file: ", latest_file)
        print("output folder: ", base_output_folder)
        output_folder = create_corresponding_folder(get_latest_folder(folder), base_output_folder)
        print("output folder: ", output_folder)

        current_time = time.time()
        if(current_time - start_time > 1200 or firstTime):
            xml_file = align_function(latest_file, output_folder + "/alignment/" + file_name_without_extension + ".xml", xml_file)
            print(xml_file)
            firstTime = False
            start_time = current_time

        print('new xml file is: ', xml_file)
        if latest_file and latest_file != latest_processed_file:
            print(f"Latest file found: {latest_file}")

            # One thread to process the file
            simplehistoThread = threading.Thread(target=makeSimpleHisto, args=(latest_file, output_folder+"/SimpleHisto/"+file_name_without_extension+".root"))

            multiTrackThread = threading.Thread(target=makeMultiTrackPlots, args=(latest_file, output_folder+"/EventDisplay/" + file_name_without_extension + ".json", xml_file))
            
            efficiencyThread = threading.Thread(target=calculateEfficiency, args=(latest_file, output_folder+ "/efficiency/" + file_name_without_extension + ".json", xml_file))
            
            singleTrack = threading.Thread(target=makeSingleTrackPlots, args=(latest_file, output_folder + "/singleTrackPlots/" + file_name_without_extension + ".root", xml_file))
            
            threads = [simplehistoThread] + [multiTrackThread] + [efficiencyThread] + [singleTrack]
            
            # Combine both lists
            #threads = [simplehistoThread] + print_threads
            
            for thread in threads:
                thread.start()

            for thread in threads:
                thread.join()

            print("All functions have been executed.")
            latest_processed_file = latest_file
            with open('signal.txt', 'w') as file:
                file.write('refresh')
        else:
            print("No new files found in the folder or no files with .root extension.")
        time.sleep(10)

if __name__ == "__main__":
    main()
