import uproot
import plotly.graph_objects as go
import numpy as np
from dash import html, dcc
import time
import os

row_style = {
    'display': 'flex',
    'flexDirection': 'row',
    'justifyContent': 'space-between',
    'marginBottom': '20px'
}

graph_style = {
    'width': '32%',
    'display': 'inline-block'
}
        
class RootFileHistograms:
    def __init__(self):
        self.root_file = None
        self.file = None
        self.histograms = {}
        self.max_retries = 3
        self.retry_delay = 5

    def _open_root_file(self, root_file):
        for attempt in range(self.max_retries):
            try:
                self.file = uproot.open(root_file)
                self.root_file = root_file
                break
            except Exception as e:
                if attempt < self.max_retries - 1:  # if it's not the last attempt
                    print(f"Error opening root file, retrying in {self.retry_delay} seconds...")
                    time.sleep(self.retry_delay)
                else:
                    raise Exception(f"Failed to open the root file '{root_file}' after {self.max_retries} attempts.") from e

    # ... [rest of the conversion functions remain unchanged]

    def load_histograms(self, root_file, histogram_names):
        self._open_root_file(root_file)
        
        for hist_name in histogram_names:
            if '2D' in hist_name:  # Assuming naming convention helps determine type
                self.histograms[hist_name] = self._convert_histogram_2d(hist_name)
            else:
                self.histograms[hist_name] = self._convert_histogram_1d(hist_name)

    def display_histogram(self, histogram_name):
        if histogram_name in self.histograms:
            self.histograms[histogram_name].show()
        else:
            print(f"Histogram {histogram_name} has not been loaded yet.")

    def _convert_histogram_1d(self, histogram_name):
        hist = self.file[histogram_name]
        counts, edges = hist.to_numpy()
        x = (edges[:-1] + edges[1:]) / 2
        fig = go.Figure(data=[go.Histogram(histfunc="sum", x=edges[:-1], y=counts, xbins=dict(start=edges[0], end=edges[-1], size=(edges[-1]-edges[0])/len(counts)))])
        fig.update_layout(title=hist.title, title_x=0.5)
        return fig

    def _convert_histogram_2d(self, histogram_name):
        hist = self.file[histogram_name]
        counts, xedges, yedges = hist.to_numpy()
        x = (xedges[:-1] + xedges[1:]) / 2
        y = (yedges[:-1] + yedges[1:]) / 2
        fig = go.Figure(data=go.Heatmap(x=x, y=y, z=counts, colorscale='Viridis'))
        fig.update_layout(title=hist.title, title_x=0.5)
        return fig

    def to_dash_graph(self, histogram_names):
        rows = []
        row_content = []
        
        for idx, histogram_name in enumerate(histogram_names):
            if histogram_name in self.histograms:
                graph = dcc.Graph(
                            id=f'graph-{histogram_name}',
                            figure=self.histograms[histogram_name],
                            style=graph_style
                        )
                row_content.append(graph)

            if (idx + 1) % 3 == 0 or (idx + 1) == len(histogram_names):
                rows.append(html.Div(row_content, style=row_style))
                row_content = []

        return rows

    def aggregate_histograms(self, root_folder, histogram_names):
        list_file = os.listdir(root_folder)
        self._open_root_file(root_folder + "/"  + list_file[0])

        dict_hist = {}
        for hist_name in histogram_names:
            hist = self.file[hist_name]
            hist_numpy = hist.to_numpy()
            dict_hist[hist_name] = hist_numpy

        for file_root in list_file[1:]:
            self._open_root_file(root_folder + "/"  + file_root)
            for hist_name in histogram_names:
                hist = self.file[hist_name]
                hist_numpy = hist.to_numpy()

                if '2D' in hist_name:
                    counts = np.concatenate((dict_hist[hist_name][0],hist_numpy[0]),axis=0)
                    xedges = dict_hist[hist_name][1]
                    yedges = dict_hist[hist_name][2]
                    dict_hist[hist_name] = [counts, xedges, yedges]

                else:
                    counts = np.concatenate((dict_hist[hist_name][0],hist_numpy[0]),axis=0)
                    edges = dict_hist[hist_name][1]
                    dict_hist[hist_name] = [counts, edges]
        
        for hist_name in histogram_names:
            if '2D' in hist_name:  # Assuming naming convention helps determine type
                counts, xedges, yedges = dict_hist[hist_name]
                x = (xedges[:-1] + xedges[1:]) / 2
                y = (yedges[:-1] + yedges[1:]) / 2
                fig = go.Figure(data=go.Heatmap(x=x, y=y, z=counts, colorscale='Viridis'))
                fig.update_layout(title=hist.title, title_x=0.5)
                self.histograms[hist_name] = fig
            else:
                counts, edges = dict_hist[hist_name]
                x = (edges[:-1] + edges[1:]) / 2
                fig = go.Figure(data=[go.Histogram(histfunc="sum", x=edges[:-1], y=counts, xbins=dict(start=edges[0], end=edges[-1], size=(edges[-1]-edges[0])/len(counts)))])
                fig.update_layout(title=hist.title, title_x=0.5)
                self.histograms[hist_name] = fig

    def empty_histogram_1d(self, histogram_names):
        counts = np.zeros(10)
        edges = np.linspace(-0.5, 9.5, num=11)
        x = np.linspace(0, 9, num=10)

        for hist_name in histogram_names:
            fig = go.Figure(data=[go.Histogram(histfunc="sum", x=edges[:-1], y=counts, xbins=dict(start=edges[0], end=edges[-1], size=(edges[-1]-edges[0])/len(counts)))])
            fig.update_layout(title=hist_name + "empty", title_x=0.5)
            self.histograms[hist_name] = fig

    def empty_histogram_2d(self, histogram_names):
        counts = np.zeros(10)
        x = np.array(0, 0, num=10)
        y = np.array(0, 0, num=10)

        for hist_name in histogram_names:
            fig = go.Figure(data=go.Heatmap(x=x, y=y, z=counts, colorscale='Viridis'))
            fig.update_layout(title=hist_name + "empty", title_x=0.5)
            self.histograms[hist_name] = fig

    def clear(self):
        self.root_file = None
        self.file = None
        self.histograms = {}