import json
import plotly.graph_objects as go
from dash import dcc, html
import os

class EfficiencyContainer:
    def __init__(self):
        self.modules_efficiency = {}
        self.loaded_files = set()  # Keep track of loaded files
        
    def load_file(self, file_path):
        # Check if the file is already loaded
        if file_path in self.loaded_files:
            print(f"File {file_path} already loaded. Skipping...")
            return
        
        with open(file_path, 'r') as f:
            data = json.load(f)

        # Iterate over the keys in the JSON file and store efficiencies
        for key, efficiency in data.items():
            module_num = int(key.split('_')[-1])
            
            if module_num not in self.modules_efficiency:
                self.modules_efficiency[module_num] = []
            
            self.modules_efficiency[module_num].append(efficiency)
        
        # After successfully loading, add the file path to the loaded files set
        self.loaded_files.add(file_path)
    
    def load_from_directory(self, dir_path):
        """Load all JSON files from the specified directory."""
        for file_name in os.listdir(dir_path):
            if file_name.endswith(".json"):
                self.load_file(os.path.join(dir_path, file_name))

    def empty_efficiency(self):
        self.modules_efficiency[0] = [0]

    def efficiency_to_dash(self):
        # Create a plotly Figure
        fig = go.Figure()
        
        # For each module, add a line to the plot
        for module_num, efficiencies in self.modules_efficiency.items():
            fig.add_trace(go.Scatter(
                y=efficiencies,
                mode='lines+markers',
                name=f"Module {module_num}"
            ))
        
        # Adjust the layout if necessary
        fig.update_layout(
            title="Module Efficiencies",
            xaxis_title="File order",
            yaxis_title="Efficiency"
        )
        
        # Return a Dash Div containing the graph
        return html.Div([dcc.Graph(figure=fig)])
    
    def to_dict(self):
        """Convert the internal state to dictionary."""
        return {
            "modules_efficiency": self.modules_efficiency,
            "loaded_files": list(self.loaded_files)
        }

    def load_from_dict(self, data_dict):
        """Load the internal state from a dictionary."""
        self.modules_efficiency = data_dict.get("modules_efficiency", {})
        self.loaded_files = set(data_dict.get("loaded_files", []))
