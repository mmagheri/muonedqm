import xml.etree.ElementTree as ET
import plotly.graph_objects as go
from dash import dcc, html
import os

class AlignParamContainer:
    def __init__(self):
        self.modules = []
        self.loaded_files = set()

    def load_from_file(self, xml_file):
        """Reads the XML file and stores module positions."""
        if xml_file in self.loaded_files:
            print(f"File {xml_file} already loaded. Skipping...")
            return
        # Parse the XML file
        tree = ET.parse(xml_file)
        root = tree.getroot()

        # Extract and store module details
        for module in root.findall('./Station/Module'):
            number = int(module.get('number'))
            name = module.get('name')
            position = module.find('Position')
            positionX = float(position.get('positionX'))
            positionY = float(position.get('positionY'))

            # Append or update the list
            module_data = next((m for m in self.modules if m['number'] == number), None)
            if module_data:
                module_data['positionsX'].append(positionX)
                module_data['positionsY'].append(positionY)
            else:
                self.modules.append({
                    'number': number,
                    'name': name,
                    'positionsX': [positionX],
                    'positionsY': [positionY]
                })
        self.loaded_files.add(xml_file)
    
    def load_from_directory(self, dir_path):
        """Load all XML files from the specified directory."""
        for file_name in os.listdir(dir_path):
            if file_name.endswith(".xml"):
                self.load_from_file(os.path.join(dir_path, file_name))

    def empty_alignment(self):
        self.modules.append({
                    'number': 0,
                    'name': 'NA',
                    'positionsX': [0],
                    'positionsY': [0]
                })
    
    def plot_positions(self, axis='X'):
        """Produces a Plotly plot for positions of modules on the selected axis."""
        
        if axis not in ['X', 'Y']:
            raise ValueError("Axis must be 'X' or 'Y'")

        fig = go.Figure()

        # Choose the correct position data based on the axis argument
        positions_key = 'positionsX' if axis == 'X' else 'positionsY'
        axis_name = "X Position" if axis == 'X' else "Y Position"

        for module in self.modules:
            fig.add_trace(go.Scatter(
                x=list(range(len(module[positions_key]))),
                y=module[positions_key],
                mode='lines+markers',
                name=f"{module['name']} {axis}"
            ))

        fig.update_layout(
            title=f"Modules' {axis_name}s",
            xaxis_title="File Sequence",
            yaxis_title=axis_name,
            legend_title="Modules"
        )

        fig.show()

    def position_to_dash(self, axis='X'):
        """Returns a dcc.Graph component with positions of modules on the selected axis."""
        
        if axis not in ['X', 'Y']:
            raise ValueError("Axis must be 'X' or 'Y'")

        fig = go.Figure()

        # Choose the correct position data based on the axis argument
        positions_key = 'positionsX' if axis == 'X' else 'positionsY'
        axis_name = "X displacement [cm]" if axis == 'X' else "Y displacement [cm]"

        for module in self.modules:
            fig.add_trace(go.Scatter(
                x=list(range(len(module[positions_key]))),
                y=module[positions_key],
                mode='lines+markers',
                name=f"{module['name']} {axis}"
            ))

        fig.update_layout(
            title=f"Modules' {axis_name}s",
            xaxis_title="File Sequence",
            yaxis_title=axis_name,
            legend_title="Modules"
        )

        return dcc.Graph(figure=fig)
    
    def positions_to_dash_div(self):
        """Returns a Dash html.Div containing two dcc.Graph components for X and Y positions side by side."""
        x_graph = self.position_to_dash('X')
        y_graph = self.position_to_dash('Y')
        
        # Create a Div containing both plots
        container = html.Div(children=[
            html.Div(x_graph, style={'width': '50%', 'display': 'inline-block'}),
            html.Div(y_graph, style={'width': '50%', 'display': 'inline-block'})
        ])
    
        return container

# Usage:
#align_param = AlignParamContainer()
#align_param.load_from_file('/muonedqm/newOutputTest/run_3090/alignment/3090_0149cf3e0_0149d33df_4201232.xml')
#align_param.load_from_file('/muonedqm/newOutputTest/run_3090/alignment/3113_1dc81b333_1dc81f332_4201232.xml')
#align_param.plot_positions('X')
#align_param.plot_positions('Y')
